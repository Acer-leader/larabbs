### Laravel 框架 踩过的坑
        1.BBS初步建立
        2.虚拟，本地同步开发
        3.语言用语言扩展包来处理 composer require "overtrue/laravel-lang:~3.0"
        
### 安装各种扩展
       -dev 表示的是只在开发环境中使用 * 代表某些文件的扩展包
       
       composer require "*/generator:~1.0" --dev 
       
       防止XXs 脚本攻击
       安装 HTMLPurifier for Laravel 5
       composer require "mews/purifier:~2.0"
       
###代码生成器使用
        php artisan make:scaffold Topic --schema="title:string:index,body:text,user_id:integer:unsigned:index,category_id:integer:unsigned:index,reply_count:integer:unsigned:default(0),view_count:integer:unsigned:default(0),last_reply_user_id:integer:unsigned:default(0),order:integer:unsigned:default(0),excerpt:text:nullable,slug:string:nullable"

### 监控mix
        npm run watch-poll
###队列任务的监控
        php artisan queue:listen
        
###回复数据的代码生成
        php artisan make:scaffold Reply --schema=
        "
        topic_id:integer:unsigned:default(0):index,
        user_id:bigInteger:unsigned:default(0):index,
        content:text
        "
        
         php artisan migrate:refresh --seed   填充数据     
        
### 消息通知系统
        //生成消息通知数据库
        php artisan notifications:table
        
###使用队列        
        //队列要重启
    php artisan queue:restart
    
### SQL错误
    外键的错误必须要 users表的id类型是int类型  那么关联表的外键必须是int        
        