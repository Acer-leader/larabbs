<footer class="footer">
    <div class="container">
        <p class="float-left">
            该论坛由 <a href="https://github.com/Acer-leader">Acer_leader</a>专用 <span style="color: #e27575;font-size: 14px;">❤</span>
        </p>
        <p class="float-right"><a href="mailto:{{ setting('contact_email') }}">联系我们</a></p>
    </div>
</footer>