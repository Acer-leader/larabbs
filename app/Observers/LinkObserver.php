<?php
/**
 *Acer_leader
 *y1wanghui@163.com
 */
namespace App\Observers;

use App\Models\Link;
use Cache;

class LinkObserver
{
    //保存数据 清空缓存 cache_key
    public function saved(Link $link)
    {
        Cache::forget($link->cache_key);
    }
}