<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        //被定义的模型才会被更改
        'name', 'description',
    ];
}
