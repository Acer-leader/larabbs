<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\MustVerifyEmail as MustVerifyEmailTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use App\Models\Topic;
use Auth;
use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable implements MustVerifyEmailContract
{
    use Traits\ActiveUserHelper;
    use Traits\LastActivedAtHelper;
    use HasRoles;
    use MustVerifyEmailTrait;
    use Notifiable {
        notify as protected laravelNotify;
    }

    // notify 重写
    public function notify($instance)
    {
        //如果要通知的人是当事人，就不必通知了
        if ( $this->id == Auth::id()){
            return;
        }

        //只有数据库类型通知才需提醒，直接发送 Email 或者其他的都 Pass
        if (method_exists($instance,'toDatabase')){
            $this->increment('notification_count');
        }

        $this->laravelNotify($instance);
    }


    public function markAsRead()
    {
        $this->notification_count = 0;
        $this->save();
        $this->unreadNotifications->markAsRead();
    }

    protected $fillable = [
        //被定义的模型才会被更改
        'name', 'email', 'password','introduction','avatar',
    ];



    protected $hidden = [
        'password', 'remember_token',
    ];



    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //模型关联

    //一个用户拥有多个话题
    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    // 用户的权限列表一个话题一个用户

    public function isAuthorOf($model)
    {
        return $this->id == $model->user_id;
    }

    //一个用户拥有多个回复

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function setPasswordAttribute($value)
    {
        //如果值得长度等于60 即认为做过加密处理
        if (strlen($value) != 60 ){
            //不等于 60 做加密处理
            $value = bcrypt($value);
        }
        $this -> attributes['password'] = $value;

    }

    public function setAvatarAttribute($path)
    {
        // 如果不是 `http` 子串开头，那就是从后台上传的，需要补全 URL
        if ( ! starts_with($path, 'http')) {
            // 拼接完整的 URL
            $path = config('app.url') . "/uploads/images/avatars/$path";
        }
        $this->attributes['avatar'] = $path;
    }

}
