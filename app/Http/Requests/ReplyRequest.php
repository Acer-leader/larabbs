<?php

namespace App\Http\Requests;

class ReplyRequest extends Request
{
    public function rules()
    {
        return[
            'content' => 'required|min:2'
        ];
    }

    public function messages()
    {
        return [
            'required|min:2'=>'内容不能为空，至少俩个字符，请勿灌水'
        ];
    }
}
