<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Link;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Models\User;

class CategoriesController extends Controller
{
    public function show(Category $category,Request $request,Topic $topic,User $user,Link $link)
    {
        //读取分类 ID 关联的列表 并按每20条分页
        $topics = $topic->withOrder($request->order)
            ->where('category_id', $category->id)
            ->paginate(20);
        //活跃用户列表
        $active_users = $user->getActiveusers();
        $links = $link->getAllCached();
        //传参变量话题和分类到模版中
        return view('topics.index',compact('topics','category','active_users','links'));
    }
}
