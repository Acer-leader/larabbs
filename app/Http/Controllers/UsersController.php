<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Handlers\ImageUploadHandler;

class UsersController extends Controller
{
    //授权登录
    public function __construct()
    {
        $this->middleware('auth',['except' => ['show']]);
    }

    //个人展示页面
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }
    //修改资料页面
    public function edit(User $user)
    {
        //授权策略
        $this->authorize('update',$user);
        return view('users.edit',compact('user'));
    }
    //更新数据动作
    public function update(UserRequest $request,ImageUploadHandler $uploader,User $user)
    {
        //dd($request->avatar); 打印数据
        $this->authorize('update',$user);
       $data = $request->all();
       if ($request->avatar){
           $result = $uploader ->save($request->avatar,'avatars',$user->id,416);
           if ($result){
               $data['avatar'] = $result['path'];
           }
       }

        $user->update($data);
        return redirect()->route('users.show',$user->id)->with('success','更新资料成功');
    }
}
