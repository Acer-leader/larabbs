<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{

    use ResetsPasswords;
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }
    //未提示密码重置提醒 重写 ResetsPasswords里面的方法 sendResetFailedResponse
    protected function sendResetResponse(Request $request, $response)
    {
        session()->flash('success','密码更新成功，您已经成功登陆!');
        return redirect($this->redirectPath());
    }



}
