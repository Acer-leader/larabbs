<?php

namespace App\Http\Middleware;

use Closure;

class EnsureEmailIsVerified
{
    /**
     * 中间件来校验用户登录 未认证Email 访问的不是用户相关的email链接
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() &&
            !$request->user()->hasVerifiedEmail() &&
            !$request->is('email/*','logout')){
            //根据客户返回对应的内容
            return $request->expectsJson()
                ? abort(403,'你的邮件地址不正确')
                : redirect()->route('verification.notice');
        }
        return $next($request);
    }
}
